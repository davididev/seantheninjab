﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// Write a CSV file and save it to memory.  See CSVTest for an example.
/// 
/// *******************************************************************************
/// EDITORS NOTE: if a string contains a ',' the comma is replaced with "&c;"  
/// This is fine for plain text files but be wary if you are using encryption code
/// as it might create a &c; somewhere where you didn't place a comma.
/// *******************************************************************************
/// </summary>
public class CSVWriter {

    private static List<string> lines = new List<string>();

    /// <summary>
    /// Reset the writing system and prepare a new file.  Should be called at the beginning of writing.
    /// </summary>
    public static void Reset()
    {
        lines.Clear();
    }

    /// <summary>
    /// Write a line to the system
    /// </summary>
    /// <param name="args"></param>
	public static void WriteLine(object[] args)
    {
        string line = "";
        for(int i = 0; i < args.Length; i++)
        {
            Type t = args[i].GetType();
            if (t.Equals(typeof(string)))
            {
                string s = (string) args[i];
                args[i] = s.Replace(",", "&c;");
            }
            if (t.Equals(typeof(float)))
            {
                float compare = (float)args[i];
                float nonDecimal = Mathf.Round(compare);
                if ((float)args[i] == nonDecimal)
                    args[i] = compare.ToString() + ".0";
            }

            line = line + args[i].ToString() + ",";
        }
        line = line.Substring(0, line.Length - 1);  //Remove the last comma

        lines.Add(line);
    }

    /// <summary>
    /// Finish the file writing system and save the file
    /// </summary>
    public static void SaveFile(string fileName, bool clearFileOnDone = true)
    {
        StreamWriter file = new StreamWriter(fileName);
        List<string>.Enumerator e1 = lines.GetEnumerator();
        while(e1.MoveNext())
        {
            Debug.Log("Writing line: " + e1.Current);
            file.WriteLine(e1.Current);
        }
        file.Close();
        Debug.Log("Wrote file: " + fileName);
        if (clearFileOnDone)
            lines.Clear();
    }
}
