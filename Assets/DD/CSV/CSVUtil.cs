﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CSVUtil 
{
    /// <summary>
    /// Return a list of bools to a string.  (For CSV saving) First character doesn't matter, it's to make the system recoginze as a string.
    /// </summary>
    /// <param name="list">The current list</param>
    /// <param name="size">The size of the list.  Creates padding if list is smaller than size.</param>
    /// <returns></returns>
    public static string BoolListToString(bool[] list, int size)
    {
        string s = "a";
        for(int i = 0; i < size; i++)
        {
            if (i < size)
                s += (list[i] == true) ? '1' : '0';
            else
                s += '0';
        }
        return s;
    }

    /// <summary>
    /// Return a bool list made from BoolListToString.
    /// </summary>
    /// <param name="s">The string created from BoolListToString</param>
    /// <param name="size">Creates padding if s is bigger than size.</param>
    /// <returns></returns>
    public static bool[] StringToBoolList(string s, int size)
    {
        bool[] yay = new bool[size];
        for(int i = 0; i < size; i++)
        {
            int x = i + 1;
            if (i < yay.Length)
                yay[i] = (s[x] == '1') ? true : false;
            else
                yay[i] = false;
        }

        return yay;
    }

    /// <summary>
    /// Create a string from a list of integers 
    /// </summary>
    /// <param name="list">the list of integers</param>
    /// <param name="size">size.  Creates padding if greater than list.</param>
    /// <returns></returns>
    public static string IntListToString(int[] list, int size)
    {
        string s = "";
        for(int i = 0; i < size; i++)
        {

            if (i < size)
                s += list[i] + " ";
            else
                s += "0 ";
        }
        return s;
    }

    /// <summary>
    /// Create an int array from string.
    /// </summary>
    /// <param name="s">The string created from IntListToString</param>
    /// <param name="size">Size of the list.  Creates padding if bigger than s</param>
    /// <returns></returns>
    public static int[] StringToIntList(string s, int size)
    {
        int[] list = new int[size];
        string[] sl = s.Split(' ');
        for(int i = 0; i < size; i++)
        {

            if (i < list.Length)
                list[i] = int.Parse(sl[i]);
            else
                list[i] = 0;
        }
        return list;
    }
    
    /// <summary>
    /// Create a string from a list of floats
    /// </summary>
    /// <param name="list">the list of float</param>
    /// <param name="size">size.  Creates padding if greater than list.</param>
    /// <returns></returns>
    public static string FloatListToString(float[] list, int size)
    {
        string s = "";
        for (int i = 0; i < size; i++)
        {
            if (i < list.Length)
            {
                if (Mathf.Approximately(Mathf.Floor(list[i]), list[i])) //Is roughly an int, add a .0
                    s += list[i] + ".0 ";
                else
                    s += list[i] + " ";
            }
            else
                s += "0.0 ";
        }
        return s;
    }

    /// <summary>
    /// Create a float array from a string.
    /// </summary>
    /// <param name="s">The string created from IntListToString</param>
    /// <param name="size">Size of the list.  Creates padding if bigger than s</param>
    /// <returns></returns>
    public static float[] StringToFloatList(string s, int size)
    {
        float[] list = new float[size];
        string[] sl = s.Split(' ');
        for (int i = 0; i < size; i++)
        {
            if (i < list.Length)
                list[i] = float.Parse(sl[i]);
            else
                list[i] = 0f;
        }
        return list;
    }
}
