﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaStar : MonoBehaviour
{
    private const float MOVE_SPEED = 12f, ROTATE_PER_SECOND = 720f, MAX_LIFE = 5f;
    private const int DMG = 5;
    private Rigidbody rigid;
    private Transform rotateable;
    private float timer;

    
    // Start is called before the first frame update
    void OnEnable()
    {
        timer = MAX_LIFE;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody>();
        if (rotateable == null)
            rotateable = transform.GetChild(0);
        rigid.velocity = transform.forward * MOVE_SPEED;



        Vector3 rot = rotateable.localEulerAngles;
        rot.y += ROTATE_PER_SECOND * Time.fixedDeltaTime;
        rotateable.eulerAngles = rot;


        timer -= Time.fixedDeltaTime;
        if (timer <= 0f)
            gameObject.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject g = collision.collider.gameObject;
        if (g.tag == "Enemy")
        {
            g.SendMessage("Damage", DMG, SendMessageOptions.DontRequireReceiver);
        }

        gameObject.SetActive(false);
    }
}
