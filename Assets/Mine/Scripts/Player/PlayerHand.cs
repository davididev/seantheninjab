﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHand : MonoBehaviour
{
    private Animator anim;
    public static bool isActive = false, hologramVisible = false, disablePause = false;
    public GameObject[] weapons;
    public GameObject ninjaStarPrefab;
    private int weaponID = 0;
    private bool firstTime = true;
    public Transform crosshair, finger;
    public Camera uiCamera;
	public AudioClip throwNinjaStarSound;
    private GameObject player;

    private LayerMask layerMaskPaused, layerMaskActive, cullingActive, cullingPaused;
    
    //after GRAPPLE_DELAY, the grapple beam retracts at (LINKS_PER_SECOND)
    

    private void Start()
    {
        if (ninjaStarPrefab != null)
            GameObjectPool.InitPoolItem("ninja star", ninjaStarPrefab, 25);
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        layerMaskPaused = LayerMask.GetMask("UI");
        layerMaskActive = LayerMask.GetMask("Default", "Enemy");
        cullingActive = LayerMask.GetMask("UI");
        cullingPaused = LayerMask.GetMask("UI", "PlayerHand");

        //GameData.instance.beams[4] = 2;  //Test grapple
        //GameData.instance.beams[0] = 2;  //Test charge beam
        //GameData.instance.beams[1] = 2;  //Test trident beam
        //Test all beams
        /*
        for (int i = 0; i < GameData.instance.beams.Length; i++)
        {
            GameData.instance.beams[i] = 1;
        }
        */

        isActive = true;
        if (firstTime)
        {
            CycleModes(0);
            InitPool();
            firstTime = false;
        }
    }


    
    public void InitPool()
    {



    
    }

    
    void CycleModes(int dir)
    {
        weaponID += dir;
        int max = weapons.Length - 1;
        if (weaponID < 0)
            weaponID = max;
        if (weaponID > max)
            weaponID = 0;


        for(int i = 0; i < weapons.Length; i++)
        {
            weapons[i].SetActive(weaponID == i);
        }
    }

    private void OnDisable()
    {
        isActive = false;
    }



    // Update is called once per frame
    void Update()
    {
        if (anim == null)
            anim = GetComponent<Animator>();

        int lm = layerMaskActive;
        if (!Mathf.Approximately(Time.timeScale, 1f))
        {
            //Paused
            lm = LayerMask.GetMask("UI");
            uiCamera.cullingMask = cullingPaused;
        }
        else
        {
            //Unpaused
            uiCamera.cullingMask = cullingActive;
        }


        //Position crosshair
        float dist = 30f;
        RaycastHit info;
        bool raycastSuccess = false;
        if (Physics.Raycast(finger.position, finger.up, out info, 30f, lm))
        {
            raycastSuccess = true;
            dist = info.distance - 0.5f;

            //Show buttons while at it
            Button b = info.collider.transform.GetComponent<Button>();
            if (b != null)
            {
                EventSystem.current.SetSelectedGameObject(b.gameObject);
                if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
                    b.onClick.Invoke();
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }

        crosshair.position = (finger.position) + (finger.up * dist);
        //crosshair.LookAt(uiCamera.transform.position);
        //LookAt rotation can f*ck up the game if two or more of the axes are 0
        Vector3 rel = crosshair.position - Camera.main.transform.position;

        Quaternion q1 = Quaternion.identity;
        if (rel != Vector3.zero)
            q1 = Quaternion.LookRotation(rel);

        crosshair.transform.rotation = q1;

        if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
        {
            if (!Mathf.Approximately(Time.timeScale, 1f/360f))
            {
                //Shiruken
                GameObject ninjaStar = GameObjectPool.GetInstance("ninja star", finger.position, Quaternion.identity);
                if(ninjaStar != null)
                {
					AudioSource.PlayClipAtPoint(throwNinjaStarSound, finger.position);
                    ninjaStar.transform.forward = finger.up;
                }
            }
        }

        if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeDownButton))
            CycleModes(1);
        if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeUpButton))
            CycleModes(-1);
    }
}

