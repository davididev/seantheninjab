﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
	public TextAsset creditsFile;
	public TMPro.TextMeshProUGUI creditsText;
	public GameObject creditsRoot;
	private Coroutine creditsRoutine;
	
    // Start is called before the first frame update
    void Start()
    {
			creditsRoot.SetActive(false);
			
    }
	
	void OnEnable()
	{
		creditsRoutine = StartCoroutine(CreditsRoutine());
	}
	
	void OnDisable()
	{
		StopCoroutine(creditsRoutine);
	}
	
	IEnumerator CreditsRoutine()
	{
		string[] lines = creditsFile.text.Split('*');
		
		while(gameObject)
		{
			for(int i = 0; i < lines.Length; i++)
			{
				creditsText.text = "Credits: \n" + lines[i];
				yield return new WaitForSecondsRealtime(10f);
			}
			
		}
		yield return new WaitForSecondsRealtime(1f);
	}
	
	public void ToggleCredits()
	{
		creditsRoot.SetActive(!creditsRoot.activeSelf);
			
	}

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void QuitGame()
	{
		Application.Quit();
	}
}
