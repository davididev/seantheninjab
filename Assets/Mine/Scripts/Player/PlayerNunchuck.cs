﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNunchuck : MonoBehaviour
{
    public Rigidbody endOfNun;
    private Coroutine c;
    // Start is called before the first frame update
    void OnEnable()
    {
        c = StartCoroutine(NunchuckForce());
    }

    IEnumerator NunchuckForce()
    {
        while(gameObject)
        {
            Vector3 rot = transform.eulerAngles;
            yield return new WaitForFixedUpdate();
            Vector3 dif =  rot - transform.eulerAngles;

            float frc = 180f / 20f;
			endOfNun.AddRelativeForce(dif * frc, ForceMode.Acceleration);
            //endOfNun.AddTorque(dif * 15f, ForceMode.Acceleration);

        }
    }

    private void OnDisable()
    {
        StopCoroutine(c);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
