﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClub : MonoBehaviour
{
    public int damageAmt = 3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            other.SendMessage("Damage", damageAmt, SendMessageOptions.DontRequireReceiver);
        }
    }
}
