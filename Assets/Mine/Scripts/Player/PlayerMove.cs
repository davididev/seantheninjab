﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public AudioClip[] jumpSounds;
    private CharacterControllerMovement ccm;
	public HeadRotator head;
	public GameObject diePrefab, pauseMenu;

    public GameObject[] enemyPrefabs;

    private int jumpID = 0;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < enemyPrefabs.Length; i++)
        {
            GameObjectPool.InitPoolItem("Enemy" + i, enemyPrefabs[i], 20);
        }
		
		GameObjectPool.InitPoolItem("Die Cloud", diePrefab, 10);
    }

    // Update is called once per frame
    void Update()
    {
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();
		
		head.transform.position = transform.position + (Vector3.up * 0.75f);
        Vector3 rot = transform.eulerAngles;
        rot.y = HeadRotator.RealFacing;
        transform.eulerAngles = rot;


		if(!Mathf.Approximately(Time.timeScale, 1f/360f))
		{
			Vector2 moveVec = Finch.FinchController.Right.TouchAxes;
			moveVec.x = Mathf.Round(moveVec.x * 2f) / 2f;
			moveVec.y = Mathf.Round(moveVec.y * 2f) / 2f;
			if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.ThumbButton))
			{
                //Jump
                if (ccm.cc.isGrounded)
                {
                    int x = Random.Range(0, 1);


                    AudioSource.PlayClipAtPoint(jumpSounds[x], transform.position);

                    Vector3 v = new Vector3(55f * moveVec.x, 25f, 55f * moveVec.y);
                    ccm.AddForce((transform.right * v.x) + (Vector3.up * v.y) + (transform.forward * v.z));
                }
			}	

			ccm.moveVec = moveVec;
		}
        if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.AppButton))
        {
            //Pause/unpause
            Time.timeScale = (Mathf.Approximately(Time.timeScale, 1f/360f)) ? 1f : 1f/360f;
            pauseMenu.SetActive(!(Mathf.Approximately(Time.timeScale, 1f)));
        }
		

    }
}
