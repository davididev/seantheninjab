﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(CharacterController))]
public class CharacterControllerMovement : MonoBehaviour
{
    
    private const float DEFAULT_GRAVITY = 32.15223f;
    public CharacterController cc;
    public Vector2 moveVec;
    public float minSpeed = 1f, maxSpeed = 3.5f, movementPerSecond = 6f, gravityScale = 1f, waterVertical = 0f;
    private float forwardSpeed, rightSpeed = 0f;
    public float timeInWater { private set; get; }

    public float rotateSpeed = 360f;
    public bool ignoreGravity = false, noMovementInAir = true, frozen = false;

    public enum ACT_IN_WATER { FLOAT, SINK };
    public ACT_IN_WATER waterAct = ACT_IN_WATER.FLOAT;

    public float currentSpeed
    {
        get
        {
            float div = 0f;
            if (!Mathf.Approximately(forwardSpeed, 0f))
                div++;
            if (!Mathf.Approximately(rightSpeed, 0f))
                div++;

            //Only divide by two if both speeds are active
            //also make sure it doesn't divide by 0
            if (Mathf.Approximately(div, 0f))
                return 0f;
            else
                return (forwardSpeed + rightSpeed) / div;
        }
    }


    public class CCForce
    {
        public const float MAX_TIME = 0.5f;
        private float time = 0f, maxTime = 0f;
        private Vector3 force;

        /// <summary>
        /// Create a force, as it sounds
        /// </summary>
        /// <param name="f">The vector of the force</param>
        public void CreateForce(Vector3 f, float t = MAX_TIME)
        {
            force = f;
            maxTime = t;
            time = t;
        }

        /// <summary>
        /// Utility function called per step.
        /// </summary>
        /// <returns></returns>
        public Vector3 ForcePerSecond()
        {
            time -= Time.deltaTime;
            if(time < 0f)
            {
                time = 0f;
                return Vector3.zero;
            }
            Vector3 v = force * (time / maxTime);
            v = v * Time.deltaTime;
            //Debug.Log("Force: " + v);
            return v;
        }

        /// <summary>
        /// Checks to see if the force is empty.
        /// </summary>
        /// <returns></returns>
        public bool isEmpty()
        {
            if (time <= 0f)
                return true;
            else
                return false;
        }
    }

    private CCForce[] forces = new CCForce[6];

    private void OnValidate()
    {
        if (GetComponent<Seeker>() == null)
            Debug.LogWarning("Seeker not found.  Pathfinding will not work. (" + gameObject.name + ")");
    }

    private Coroutine pathfindingCoroutine;
    Path path;
    public void SetDestination(Vector3 v)
    {
        Seeker s = GetComponent<Seeker>();
        Path p = s.StartPath(transform.position, v);
        if (p.error == false)
        {
            path = p;
            if (pathfindingCoroutine != null)
                StopCoroutine(pathfindingCoroutine);
            pathfindingCoroutine = StartCoroutine(Pathfinding());
        }
    }

    public void TurnTowards(Vector3 pos)
    {
        Vector3 rel = pos - transform.position;
        float targetRot = Quaternion.LookRotation(rel).eulerAngles.y;
        Vector3 rot = transform.eulerAngles;
        rot.y = Mathf.MoveTowardsAngle(rot.y, targetRot, rotateSpeed * Time.deltaTime);
        transform.eulerAngles = rot;
    }

    public bool pathActive = false;
    IEnumerator Pathfinding()
    {
        pathActive = true;

        
        for(int i = 0; i < path.vectorPath.Count; i++)
        {

            Vector3 d = path.vectorPath[i];
            while (Vector3.Distance(transform.position, d) > 1f)
            {
                moveVec.Set(0f, 1f);
                

                TurnTowards(path.vectorPath[i]);
                

                yield return new WaitForEndOfFrame();
            }
        }
        pathActive = false;
        yield return null;
    }

    // Start is called before the first frame update
    void Start()
    {
        timeInWater = 0f;
        if (cc == null)
            cc = GetComponent<CharacterController>();

        for(int i = 0; i < forces.Length; i++)
        {
            forces[i] = new CCForce();
        }
    }
    float gravity = 0f;

    Vector3 startPos;
    private void OnEnable()
    {
        startPos = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        float gs = gravityScale;  //When we have a global gravity scale, put it here.
        if (cc == null)
            cc = GetComponent<CharacterController>();
        if(frozen)
        {
            gravity = 0f;
            return;
        }
        //Process forces
        Vector3 overallForce = Vector3.zero;
        for(int i = 0; i < forces.Length; i++)
        {
            overallForce += forces[i].ForcePerSecond();
        }
        float headY = cc.transform.position.y + (cc.height * 0.45f);
        //Process gravity
        if (cc.isGrounded && gravity >= 0f)
        {
            if(waterAct == ACT_IN_WATER.FLOAT)
                gravity = 0.1f * gs;
        }
        else
            gravity += DEFAULT_GRAVITY * Time.deltaTime * gs;
        overallForce += (Vector3.down * gravity * Time.deltaTime);

    
        //Process movement
        if (moveVec.y != 0f)
        {
            forwardSpeed += (moveVec.y * movementPerSecond * Time.deltaTime);
            if (Mathf.Abs(forwardSpeed) < minSpeed)
            {
                if (forwardSpeed < 0f)
                    forwardSpeed = -minSpeed;
                else
                    forwardSpeed = minSpeed;
            }
            if (Mathf.Abs(forwardSpeed) > maxSpeed)
            {
                if (forwardSpeed < 0f)
                    forwardSpeed = -maxSpeed;
                else
                    forwardSpeed = maxSpeed;
            }

        }
        else
            forwardSpeed = Mathf.MoveTowards(forwardSpeed, 0f, movementPerSecond * 2f * Time.deltaTime);
        if (moveVec.x != 0f)
        {
            rightSpeed  += (moveVec.x * movementPerSecond * Time.deltaTime);
            if (Mathf.Abs(rightSpeed) < minSpeed)
            {
                if (rightSpeed < 0f)
                    rightSpeed = -minSpeed;
                else
                    rightSpeed = minSpeed;
            }
            if (Mathf.Abs(rightSpeed) > maxSpeed)
            {
                if (rightSpeed < 0f)
                    rightSpeed = -maxSpeed;
                else
                    rightSpeed = maxSpeed;
            }

        }
        else
            rightSpeed = Mathf.MoveTowards(rightSpeed, 0f, movementPerSecond * 2f * Time.deltaTime);

        overallForce += (transform.forward * forwardSpeed * Time.deltaTime) + (transform.right * rightSpeed * Time.deltaTime);


        cc.Move(overallForce);

    }

    public void AddForce(Vector3 v, float time = CCForce.MAX_TIME)
    {
        for(int i = 0; i < forces.Length; i++)
        {
            if(forces[i].isEmpty())
            {
                forces[i].CreateForce(v);
                return;
            }
        }

        Debug.LogWarning("Could not add force.  :(");
    }
	
	public void Jump(float h, bool onlyIfGrounded = true)
	{
		if(onlyIfGrounded == true && cc.isGrounded == false)
			return;
		
		gravity = -h;
	}

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(hit.gameObject.tag == "Water")
        {
            transform.position = startPos;
        }
		
            hit.collider.gameObject.SendMessage("ControlHit", new ControllerCollisionMessage(gameObject, hit), SendMessageOptions.DontRequireReceiver);
        
    }
	
}
