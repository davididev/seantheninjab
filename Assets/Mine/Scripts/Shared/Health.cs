﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public AudioClip damageSound;
    public int startingHealth = 4;
    private int currentHealth = 4;
    public Light damageLight;
    private float lightTimer = 0f;
    // Start is called before the first frame update
    void OnEnable()
    {
		
        currentHealth = startingHealth;
    }
	
	

    // Update is called once per frame
    void Update()
    {
        


        if (lightTimer > 0f)
        {
            lightTimer -= Time.deltaTime;
            if(lightTimer <= 0f)
            {
                damageLight.enabled = false;
            }
        }
    }

    void Damage(int x)
    {
        if (damageSound != null)
            AudioSource.PlayClipAtPoint(damageSound, transform.position);
        damageLight.enabled = true;
        lightTimer = 0.15f;
        currentHealth -= x;

        if (currentHealth <= 0)
		{
			GameObject g = GameObjectPool.GetInstance("Die Cloud", transform.position, Quaternion.identity);
            gameObject.SetActive(false);
		}
    }
}
