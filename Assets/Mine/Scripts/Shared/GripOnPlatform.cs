﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GripOnPlatform : MonoBehaviour {

	public Animator animToEffect;
    private Transform player;
    private int updates = 0;
    private Vector3 lastPlayerPosition, lastRealPos;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (player != null)
        {
			if(animToEffect != null)
				animToEffect.SetTrigger("Grounded");
            //Make sure the player is remembered for two frame updates

			if(lastPlayerPosition != Vector3.zero)
				MoveAfterFramestep();

			lastRealPos = player.position;
            lastPlayerPosition = transform.InverseTransformPoint(player.position);
            if (updates >= 2)
            {
				lastPlayerPosition = Vector3.zero;
                player = null;
            }
            updates++;
        }
	}

    private void MoveAfterFramestep()
    {
        //Move the player AFTER the framestep
        if(player != null)
        {
			Debug.Log("Player's touching me!");
            CharacterController cc = player.GetComponent<CharacterController>();
            /*
			cc.enabled = false;
            Vector3 newPos = transform.TransformPoint(lastPlayerPosition);
            player.transform.position = newPos;
            cc.enabled = true;
			*/
			
			Vector3 newPos = transform.TransformPoint(lastPlayerPosition);
			
			Vector3 rel = newPos - lastRealPos + (Vector3.down * 0.05f);
			cc.Move(rel);
        }
    }

    void ControlHit(ControllerCollisionMessage hit)
    {
		if(hit.caster.tag == "Player")
		{
        player = hit.caster.transform;
        updates = 0;
		}
    }
}
