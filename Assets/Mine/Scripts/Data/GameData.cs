﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData 
{
	public static GameData instance = new GameData();
	public static int fileID = 0;
	
	public int[] beams = {0, 0, 0, 0, 0};  //Charge(0), trident(1), wave(2), plasma(3), and grapple(4).
	public bool[] powerups, doors;
	public bool[] worldAMap, worldBMap, worldCMap, worldDMap, worldEMap;
    public int missiles = 0, maxMissiles = 0, superMissiles = 0, maxSuperMissiles = 0, healthContainers = 0;
	public int maxHealth
	{
		get
		{
			int h = 20;
			h += (healthContainers * 20);
			return h;
		}
	}
	public bool crouch = false, doubleJump = false, screwAttack = false;
	public int minHealth = 20;
	public string sceneName = "";
	public Vector3 currentPosition;
	public float currentRotation = 0f;
	
	public static string GetAreaName(char worldID)
	{
		if(worldID == 'A')
			return "Nirdara";  //Sanskrit for 'Cavern'
		if(worldID == 'B')
			return "Kacchabhu";  //Sanskrit for 'Swamp'
		if(worldID == 'C')
			return "Agniparvata";  //Sanskrit for 'Volcano.'
		if(worldID == 'D')
			return "Mahodadhi";  //Sanskrit for 'Ocean'
		if(worldID == 'E')
			return "Durga"; //Sanskrit for 'Fortress'
		//706-310-4357
		//
		return "New file";
	}
	
	public GameData()
	{
		
		worldAMap = new bool[20];
		worldBMap = new bool[20];
		worldCMap = new bool[20];
		worldDMap = new bool[20];
		worldEMap = new bool[20];
		powerups = new bool[100];
		doors = new bool[25];
		//Note to self:
		//Camera.main.activeTexture 
		//I wanna use that for a screenshot to test it out.
		if(Load() == false)
			NewFile(fileID);
		
	}
	
	public void NewFile(int f)
	{
		//sceneName = "Tutorial";  //This should be the real one
		sceneName = "A1";  //A1 is the temporary prototype.  It will also be a real area
		currentPosition = Vector3.zero;
		currentRotation = 0f;
		missiles = 0;
		superMissiles = 0;
		healthContainers = 0;
		
		for(int i = 0; i < beams.Length; i++)
		{
			beams[i] = 0;
		}
		
		
		for(int i = 0; i < powerups.Length; i++)
		{
			powerups[i] = false;
		}
		for(int i = 0; i < doors.Length; i++)
		{
			doors[i] = false;
		}
		
		for(int i = 0; i < worldAMap.Length; i++)
		{
			worldAMap[i] = false;
		}
		for(int i = 0; i < worldBMap.Length; i++)
		{
			worldBMap[i] = false;
		}
		for(int i = 0; i < worldCMap.Length; i++)
		{
			worldCMap[i] = false;
		}
		for(int i = 0; i < worldDMap.Length; i++)
		{
			worldDMap[i] = false;
		}
		for(int i = 0; i < worldEMap.Length; i++)
		{
			worldEMap[i] = false;
		}
		
	}
	
	public void Save()
	{
		CSVWriter.Reset();
		CSVWriter.WriteLine(new object[] { "beams", CSVUtil.IntListToString(beams, beams.Length) });
		CSVWriter.WriteLine(new object[] { "powerups", CSVUtil.BoolListToString(powerups, powerups.Length) });
		CSVWriter.WriteLine(new object[] { "doors", CSVUtil.BoolListToString(doors, doors.Length) });
		CSVWriter.WriteLine(new object[] { "stats", minHealth, healthContainers, missiles, superMissiles, crouch, doubleJump, screwAttack, maxMissiles, maxSuperMissiles });
		CSVWriter.WriteLine(new object[] { "location", sceneName, currentPosition.x, currentPosition.y, currentPosition.z, currentRotation });
		CSVWriter.WriteLine(new object[] { "maps", 
							CSVUtil.BoolListToString(worldAMap, worldAMap.Length),
							CSVUtil.BoolListToString(worldBMap, worldBMap.Length),
							CSVUtil.BoolListToString(worldCMap, worldCMap.Length),
							CSVUtil.BoolListToString(worldDMap, worldDMap.Length),
							CSVUtil.BoolListToString(worldEMap, worldEMap.Length)
							});
	}
	
	public bool Load()
	{
		string[] lines = CSVReader.GetFile(Application.persistentDataPath + "/file" + fileID + ".csv");
        if (lines == null)
        {
            return false;
        }
		for(int i = 0; i < lines.Length; i++)
		{
			object[] args = CSVReader.GetArgumentsInLine(lines[i]);
			if((string) args[0] == "beams")
			{
				int[] list = CSVUtil.StringToIntList((string) args[1], beams.Length);
				for(int x = 0; x < list.Length; x++)
				{
					beams[x] = list[x];
				}
			}
			if((string) args[0] == "powerups")
			{
				bool[] list = CSVUtil.StringToBoolList((string) args[1], powerups.Length);
				for(int x = 0; x < list.Length; x++)
				{
					powerups[x] = list[x];
				}
			}
			if((string) args[0] == "doors")
			{
				bool[] list = CSVUtil.StringToBoolList((string) args[1], doors.Length);
				for(int x = 0; x < list.Length; x++)
				{
					doors[x] = list[x];
				}
			}
			if((string) args[0] == "stats")
			{
				minHealth = (int) args[1];
				healthContainers = (int) args[2];
				missiles = (int) args[3];
				superMissiles = (int) args[4];
				crouch = (bool) args[5];
				doubleJump = (bool) args[6];
				screwAttack = (bool) args[7];
                maxMissiles = (int)args[8];
                maxSuperMissiles = (int)args[9];
			}
			
			if((string) args[0] == "location")
			{
				sceneName = (string) args[1];
				float x = (float) args[2];
				float y = (float) args[3];
				float z = (float) args[4];
				float rot = (float) args[5];
				currentRotation = rot;
				currentPosition.Set(x, y, z);
			}
			if((string) args[0] == "maps")
			{
				bool[] w1 = CSVUtil.StringToBoolList((string) args[1], worldAMap.Length);
				for(int x = 0; x < w1.Length; x++)
				{
					worldAMap[x] = w1[x];
				}
				bool[] w2 = CSVUtil.StringToBoolList((string) args[2], worldBMap.Length);
				for(int x = 0; x < w2.Length; x++)
				{
					worldBMap[x] = w2[x];
				}
				bool[] w3 = CSVUtil.StringToBoolList((string) args[3], worldCMap.Length);
				for(int x = 0; x < w3.Length; x++)
				{
					worldCMap[x] = w3[x];
				}
				bool[] w4 = CSVUtil.StringToBoolList((string) args[4], worldDMap.Length);
				for(int x = 0; x < w4.Length; x++)
				{
					worldDMap[x] = w4[x];
				}
				bool[] w5 = CSVUtil.StringToBoolList((string) args[5], worldEMap.Length);
				for(int x = 0; x < w5.Length; x++)
				{
					worldEMap[x] = w5[x];
				}
			}
		}
		return true;
	}
}
