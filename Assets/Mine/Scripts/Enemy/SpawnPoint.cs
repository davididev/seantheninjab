﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public float delayVar = 0f;

    private const float DELAY_BASE = 10f, DELAY_PER_VAR = 12f;
    private float timer = 0f;

    private bool isVisible = false;

    private static int spawnID = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (Mathf.Approximately(delayVar, 0f))
            Invoke("SpawnEnemy", 0.1f);
    }

    private void OnBecameVisible()
    {
        isVisible = true;
    }

    private void OnBecameInvisible()
    {
        isVisible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!isVisible)
        {
            timer += Time.deltaTime;
            if (timer > (DELAY_BASE + (DELAY_PER_VAR * delayVar)))
            {
                timer = 0f;
                SpawnEnemy();
            }
        }
        
    }

    void SpawnEnemy()
    {
        
        if(Enemy.enemyCount < 10)
        {
            if (spawnID >= 0 && spawnID < 10)
            {
                GameObjectPool.GetInstance("Enemy0", transform.position, Quaternion.identity);
            }
            if(spawnID >= 10 && spawnID < 30)
            {
                Random.InitState(spawnID);
                int x = Random.Range(0, 1);
                x = 0;  //Comment this out when we get more
                GameObjectPool.GetInstance("Enemy" + x, transform.position, Quaternion.identity);
            }
            if (spawnID >= 30)
            {
                Random.InitState(spawnID);
                int x = Random.Range(0, 2);
                x = 0;  //Comment this out when we get more
                GameObjectPool.GetInstance("Enemy" + x, transform.position, Quaternion.identity);
            }
            spawnID++;
        }
    }
}
