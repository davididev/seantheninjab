﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : Enemy
{
    
    public override IEnumerator EnemyScript()
    {
        yield return new WaitForSeconds(1f);
        
        SetBrain(BRAIN.Patrol);
       playerFound = false;
        while (playerFound == false)
        {
            yield return new WaitForEndOfFrame();            
        }
        Debug.Log("Following player");
        SetBrain(BRAIN.FollowPlayer);
        

    }
}
