﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static int enemyCount = 0;
    protected CharacterControllerMovement ccm;
    protected Coroutine currentBrain, mainRoutine;
    protected bool playerFound;
    public enum BRAIN { NONE, FollowPlayer, Patrol}
	
	public int touchDamage = 2;
	private float damageTimer = 0f;
	private const float DAMAGE_DELAY = 0.25f;

    // Start is called before the first frame update
    void OnEnable()
    {
        enemyCount++;
        playerFound = false;
		if(mainRoutine != null)
			StopCoroutine(mainRoutine);
        mainRoutine = StartCoroutine(EnemyScript());
        OnStart(); 
    }
    

    public virtual IEnumerator EnemyScript()
    {
        yield return null;
    }

    public virtual void OnStart()
    {

    }

    private void OnDisable()
    {
        StopCoroutine(mainRoutine);
        enemyCount--;
    }

    public void SetBrain(BRAIN b)
    {
        if (currentBrain != null)
            StopCoroutine(currentBrain);
        if(b == BRAIN.FollowPlayer)
            currentBrain = StartCoroutine(BrainFollow());
        if (b == BRAIN.Patrol)
            currentBrain = StartCoroutine(BrainPatrol());
    }

    IEnumerator BrainFollow()
    {
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        while (gameObject)
        {
            if (Vector3.Distance(transform.position, player.transform.position) > 20f)
            {
                ccm.SetDestination(player.transform.position);
                yield return new WaitForSeconds(2.5f);
            }
            else
            {
                ccm.moveVec.Set(0f, 1f);
                ccm.TurnTowards(player.transform.position);
            }
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }


    IEnumerator BrainPatrol()
    {
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();

        GameObject pathRoot = GameObject.FindGameObjectWithTag("PatrolPath");
        if (pathRoot != null)
        {
            int i = 0;
            for(int x = 0; x < pathRoot.transform.childCount; x++)
            {
                Vector3 p = pathRoot.transform.GetChild(x).position;
                if(Vector3.Distance(p, transform.position) < 4f)
                {
                    i = x;
                    break;
                }
            }
            while (gameObject)
            {
                //for (int i = 0; i < pathRoot.transform.childCount; i++)
                //{
                    ccm.SetDestination(pathRoot.transform.GetChild(i).position);
                    while (ccm.pathActive == true)
                    {
                        yield return new WaitForSeconds(2.5f);
                        ccm.SetDestination(pathRoot.transform.GetChild(i).position);
                    }
                //}
                i++;
                if (i >= pathRoot.transform.childCount)
                    i = 0;
                yield return new WaitForEndOfFrame();
            }
        }
        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        if (ccm == null)
            ccm = GetComponent<CharacterControllerMovement>();

        if(playerFound == false)
        {
            Collider[] c = Physics.OverlapSphere(transform.position, 12f, LayerMask.GetMask("Player"));
            if (c != null && c.Length > 0)
                playerFound = true;
        }
        OnUpdate();
    }

    public virtual void OnUpdate()
    {

    }
}
