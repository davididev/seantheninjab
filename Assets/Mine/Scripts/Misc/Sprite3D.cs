﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Attach this to a Quad (preferably a quad) to create a retro-style sprite 
/// in 3D space.
/// 
/// Locking refers to the rotation eulers.  For instance, if you want a Doom-style
/// sprite that stays upright, lock everything but Y.
/// 
/// The variables currentTexutre and currentColor should be set by an Animator component.
/// </summary>
public class Sprite3D : MonoBehaviour {

    public bool lockX = false;
    public bool lockY = false;
    public bool lockZ = false;
    private Transform head;

    public Texture currentTexture;
	private Texture lastTexture;

	public Color currentColor = Color.white;
	private Color lastColor = Color.white;

    private float textureTimer = 0f;
    MeshRenderer rend;

    // Use this for initialization
    void Start () {
        rend = GetComponent<MeshRenderer>();
        
        //head = Camera.main.transform;
    }
    /*
    void OnValidate()
    {
        MeshRenderer rend = GetComponent<MeshRenderer>();
        if (texture != null && rend != null)
            rend.material.mainTexture = texture;
    }
	*/
	// Update is called once per frame
	void Update () {
        if (head == null && Camera.main != null)
            head = Camera.main.transform;

        if (head != null)
        {
			Vector3 rel = transform.position - head.position;
            Quaternion q1 = Quaternion.identity;
			if (rel != Vector3.zero)
				q1 = Quaternion.LookRotation(rel);
            
            Vector3 currentAngles = transform.eulerAngles;
            Vector3 newAngles = q1.eulerAngles;

            //If any axises are locked, don't rotate (go back to the angles BEFORE you rotated)
            if (lockX)
                newAngles.x = currentAngles.x;
            if (lockY)
                newAngles.y = currentAngles.y;
            if (lockZ)
                newAngles.z = currentAngles.z;

            transform.eulerAngles = newAngles;
        }
        if (currentTexture != null)
		{
			if(currentTexture != lastTexture)
			{
				rend.material.mainTexture = currentTexture;
				lastTexture = currentTexture;
			}
		}
		
		if(currentColor != lastColor)
		{
			rend.material.color = lastColor;
			lastColor = currentColor;
		}
    }
}
