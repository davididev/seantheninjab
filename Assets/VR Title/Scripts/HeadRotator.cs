﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadRotator : MonoBehaviour
{
    private const float ROTATE_PER_SECOND = 90f;
    [SerializeField] public Transform mainCamera, uiCamera, uiHead;
    public static float RealFacing = 0f;  //Accounting for VR rotation and head rotation
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

#if UNITY_STANDALONE || UNITY_EDITOR
        //Only rotate by tilting phone in editor.
        float rot = Input.GetAxis("Horizontal");
        Vector3 rotVec = transform.eulerAngles;
        if (rot > 0.25f)
            rotVec.y += ROTATE_PER_SECOND * Time.deltaTime;
        if (rot < -0.25f)
            rotVec.y -= ROTATE_PER_SECOND * Time.deltaTime;

        float xVec = Input.GetAxis("Vertical");
        //rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, xVec, ROTATE_PER_SECOND * Time.deltaTime);
        if (xVec < 0f)
            rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, 80f, ROTATE_PER_SECOND * Time.deltaTime);
        if (xVec > 0f)
            rotVec.x = Mathf.MoveTowardsAngle(rotVec.x, -80f, ROTATE_PER_SECOND * Time.deltaTime);
        transform.eulerAngles = rotVec;
#endif
        RealFacing = mainCamera.eulerAngles.y;
    }
}
