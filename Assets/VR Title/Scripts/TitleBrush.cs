﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TitleBrush : MonoBehaviour
{
    public static bool isWorking = false;
    public static Color color = Color.white;
    public MeshRenderer brushMaterial;

    private int colorID = 0;
    private const int MAX_COLOR = 9;
    public Transform startingPoint, crosshair;

    // Start is called before the first frame update
    void OnEnable()
    {
        isWorking = true;
        UpdateColor();
    }
    private void OnDisable()
    {
        isWorking = false;
    }
    public void UpdateColor()
    {
        Color c = Color.black;
        if (colorID == 1)
            c = Color.white;
        if (colorID == 2)
            c = Color.green;
        if (colorID == 3)
            c = Color.red;
        if (colorID == 4)
            c = Color.blue;
        if (colorID == 5)
            c = Color.green;
        if (colorID == 6)
            c = Color.cyan;
        if (colorID == 7)
            c = Color.magenta;
        if (colorID == 8)
            c = Color.yellow;
        if (colorID == 9)
            c = new Color(1f, 0.5f, 0f, 1f);  //Orange
        color = c;
        brushMaterial.materials[1].color = c;
    }

    // Update is called once per frame
    void Update()
    {
        if(Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeDownButton))
        {
            colorID++;
            if (colorID > MAX_COLOR)
                colorID = 0;

            UpdateColor();
        }
        if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.VolumeUpButton))
        {
            colorID--;
            if (colorID < 0)
                colorID = MAX_COLOR;

            UpdateColor();
        }

        RaycastHit info;
        float distance = 25f;
        if (Physics.Raycast(startingPoint.position, startingPoint.forward, out info, distance, LayerMask.GetMask("Water", "UI")))
        {
            distance = info.distance - 1f;
            if (Finch.FinchController.Right.GetPress(Finch.FinchControllerElement.Trigger))
            {

                NewFileGrid grid = info.collider.GetComponent<NewFileGrid>();
                if (grid != null)
                {
                    grid.SetPixel(info.textureCoord);
                }
            }

            Button b = info.collider.GetComponent<Button>();
            if (b != null)
            {
                EventSystem.current.SetSelectedGameObject(b.gameObject);
                if (Finch.FinchController.Right.GetPressDown(Finch.FinchControllerElement.Trigger))
                    b.onClick.Invoke();
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }

        crosshair.position = (startingPoint.position) + (startingPoint.forward * distance);
        crosshair.LookAt(Camera.main.transform.position);
    }

        
}
