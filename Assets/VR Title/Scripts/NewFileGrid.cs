﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewFileGrid : MonoBehaviour
{
    
    private Texture2D t;
    private MeshRenderer rend;
    // Start is called before the first frame update
    void OnEnable()
    {
        t = new Texture2D(32, 32);
        t.filterMode = FilterMode.Point;
        GetComponent<MeshRenderer>().material.mainTexture = t;
    }

    public void SetPixel(Vector2 uv)
    {
        if(rend == null)
            rend = GetComponent<MeshRenderer>();
        t = (Texture2D) rend.material.mainTexture;
        int x = Mathf.FloorToInt(uv.x * 32f);
        int y = Mathf.FloorToInt(uv.y * 32f);
        if (x > t.width - 1)
            x = t.width - 1;
        if (x < 0)
            x = 0;
        if (y > t.height - 1)
            y = t.height - 1;
        if (y < 0)
            y = 0;

        Debug.Log("UV is " + uv + "New pos: " + x +"," + y);
        t.SetPixel(x, y, TitleBrush.color);
        t.Apply();
        //rend.material.mainTexture = t;
         
    }

    public void SaveFile()
    {
        Debug.Log("Saving file");
        //int id = 0;   
        int id = GameData.fileID;
        t = (Texture2D)rend.material.mainTexture;
        byte[] file = t.EncodeToPNG();
        System.IO.File.WriteAllBytes((Application.persistentDataPath + "/file" + id + ".png"), file);
        GameData.instance.NewFile(id);
        GameData.instance.Save();
        FindObjectOfType<VRTitle>().SetPanelID(0);
    }

    public void Cancel()
    {
        //transform.root.gameObject.SetActive(true);
        FindObjectOfType<VRTitle>().SetPanelID(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
