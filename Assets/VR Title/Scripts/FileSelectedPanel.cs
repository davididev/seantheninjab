﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FileSelectedPanel : MonoBehaviour
{
    public GameObject mainPanel, confirmDelete;
    public TMPro.TextMeshProUGUI fileStatus;
    int missionID = 0;
    // Start is called before the first frame update
    void OnEnable()
    {
        GameData.instance.Load();
		int totalPickups = GameData.instance.powerups.Length + GameData.instance.beams.Length;
		int unlockedPickups = 0;
		for(int i = 0; i < GameData.instance.powerups.Length; i++)
		{
			if(GameData.instance.powerups[i] == true)
				unlockedPickups++;
		}
		for(int i = 0; i < GameData.instance.beams.Length; i++)
		{
			if(GameData.instance.beams[i] > 0)
				unlockedPickups++;
		}
		
		int perc = unlockedPickups * 100 / totalPickups;
		string s = GameData.GetAreaName(GameData.instance.sceneName[0]);
		fileStatus.text = "Current Area: " + s +"\nProgress: " + perc + "%";
		
		/*
        int totalScore = 0;
        for(int i = 0; i < GameData.instance.highScores.Length; i++)
        {
            totalScore += GameData.instance.highScores[i];
        }
        chapter1Text.text = chapter1Text.text.Replace("%s", totalScore.ToString() + "/600");

        totalScore = 0;
        for (int i = 0; i < GameData.instance.highScores2.Length; i++)
        {
            totalScore += GameData.instance.highScores2[i];
        }
        chapter2Text.text = chapter2Text.text.Replace("%s", totalScore.ToString() + "/700");
        SetPanelID(0);
		*/
    }

    public void Continue()
    {
        //LoadingUI.sceneID = GameData.instance.sceneName;
        //UnityEngine.SceneManagement.SceneManager.LoadScene("Loading");
		SceneManager.LoadScene(GameData.instance.sceneName);
		FindObjectOfType<VRTitle>().gameObject.SetActive(false);
    }


    public void SetPanelID(int id)
    {
        mainPanel.SetActive(id == 0);
        confirmDelete.SetActive(id == 1);
        //confirmMission.SetActive(id >= 2 && id <= 3);
    }

    public void GoBack()
    {
        FindObjectOfType<VRTitle>().SetPanelID(0);
    }
    
    public void DeleteFile()
    {
        int id = GameData.fileID;
        string path1 = Application.persistentDataPath + "/file" + id + ".png";
        string path2 = Application.persistentDataPath + "/file" + id + ".csv";
        File.Delete(path1);
        if (File.Exists(path2))
            File.Delete(path2);
		mainPanel.SetActive(true);
		confirmDelete.SetActive(false);
        FindObjectOfType<VRTitle>().SetPanelID(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
