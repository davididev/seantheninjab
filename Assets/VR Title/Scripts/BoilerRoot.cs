﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoilerRoot : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        Object.DontDestroyOnLoad(transform);
    }

    public static void DestroyMe()
    {
		BoilerRoot br = FindObjectOfType<BoilerRoot>();
		if(br != null)
			Destroy(br.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
